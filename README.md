# gitbash_solarized
Solarized (dark) Git Bash and VIM

This is how I solarize the Git Bash terminal and vim and change the font, prompt and title. 

# Installation

Clone project and run setup.sh

<i>Note: existing files if any will be backed up.</i>

Or manually add or make changes to the following files/folders:
~/.bashrc
~/.minttyrc
~/.vimrc
~/.dir_colors
~/.vim/colors/solarized.vim

If you clone this repo, make sure you do "ls -a" to see the hidden files.

# Credits
The solarized color scheme was designed by Ethan Schoonover. More info here:</br>
http://ethanschoonover.com/solarized</br>

dir_colors was found here:</br>
https://github.com/seebi/dircolors-solarized/blob/master/dircolors.ansi-dark</br>

The vim solarized color scheme was found here:</br>
https://github.com/altercation/vim-colors-solarized/blob/master/colors/solarized.vim</br>
